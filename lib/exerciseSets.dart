import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:test_app_flutter/drawer.dart';
import 'package:test_app_flutter/editExerciseSet.dart';
import 'package:test_app_flutter/style.dart';
import 'package:timeago/timeago.dart' as timeago;

import 'generated/l10n.dart';
import 'globals.dart' as globals;

class ExerciseSets extends StatefulWidget {
  @override
  _ExerciseSetsState createState() => _ExerciseSetsState();
}

class _ExerciseSetsState extends State<ExerciseSets>
    with TickerProviderStateMixin {
  bool _loadedTests = false;
  List _joinTests;
  ScrollController _scrollController = ScrollController();

  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    globals.api
        .call('listJoin', options: {'showAll': true}, context: context)
        .then((data) {
      if (data['response'] is List) {
        _joinTests = data['response'].reversed.toList();
      } else
        _joinTests = [];
      setState(() {
        _loadedTests = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (ResponsiveDrawerScaffold(
      helpPage: HelpPage(
          path: HelpPagePath.joinTests, label: S.of(context).helpWithJoinTests),
      title: S.of(context).joinTests,
      appBarBottom: TabBar(
        tabs: [
          Tab(
            child: Text(S.of(context).own),
          ),
          Tab(
            child: Text(S.of(context).all),
          )
        ],
        controller: _tabController,
      ),
      body: TestAppScrollBar(
          controller: _scrollController,
          child: (_loadedTests)
              ? (_joinTests.isNotEmpty)
                  ? TabBarView(
                      controller: _tabController,
                      children: [
                        SingleChildScrollView(
                          controller: _scrollController,
                          child: ExerciseSetListView(
                              exerciseSets: _joinTests, onlyOwn: true),
                        ),
                        SingleChildScrollView(
                          controller: _scrollController,
                          child: ExerciseSetListView(
                              exerciseSets: _joinTests, onlyOwn: false),
                        ),
                      ],
                    )
                  : ListTile(
                      leading: FaIcon(FontAwesomeIcons.infoCircle),
                      title: Text(S.of(context).thereAreNoJoinTestsYet),
                    )
              : CenterProgress()),
      floatingActionButton: FloatingActionButton(
        tooltip: S.of(context).createNewJoinTest,
        child: FaIcon(FontAwesomeIcons.plus),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (b) => EditExerciseSet()));
        },
      ),
    ));
  }
}

class ExerciseSetListView extends StatelessWidget {
  final Iterable exerciseSets;
  final bool onlyOwn;

  const ExerciseSetListView({Key key, this.exerciseSets, this.onlyOwn})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    List filteredExerciseSets = (onlyOwn)
        ? exerciseSets.toList().where((element) {
            return int.parse(element['admin']) ==
                int.parse(globals.userInfo['response']['id']);
          }).toList()
        : exerciseSets.toList();
    return (filteredExerciseSets.isEmpty)
        ? ListTile(
            leading: FaIcon(FontAwesomeIcons.info),
            title: Text(S.of(context).youDidntCreateAnyExerciseSetYet),
            trailing: OutlinedButton(
              child: Text(S.of(context).createNewJoinTest),
              onPressed: () => Navigator.of(context)
                  .push(MaterialPageRoute(builder: (c) => EditExerciseSet())),
            ),
          )
        : Wrap(
            alignment: WrapAlignment.spaceEvenly,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: filteredExerciseSets.map<Widget>((element) {
              return GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => EditExerciseSet(
                            id: int.parse(element['id']),
                            testData: element,
                          )));
                },
                child: (Card(
                  color: colorByText(element['subjects'].toList()[0]),
                  child: DefaultTextStyle.merge(
                    style: TextStyle(color: Colors.white),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            element['subjects'].join(", "),
                            style: Theme.of(context)
                                .textTheme
                                .subtitle1
                                .copyWith(color: Colors.white),
                          ),
                          Container(
                            constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width / 3),
                            child: Text(
                              element['name'],
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(color: Colors.white),
                            ),
                          ),
                          Text(S.of(context).topics),
                          Container(
                            constraints: BoxConstraints(
                                maxWidth:
                                    MediaQuery.of(context).size.width / 3),
                            child: Wrap(
                                spacing: 8,
                                children: (element['fullTopics'] as List)
                                    .map<Widget>((topic) {
                                  return (Chip(label: Text(topic['name'])));
                                }).toList()),
                          ),
                          Text(timeago.format(
                              DateTime.fromMillisecondsSinceEpoch(
                                  int.parse(element['timestamp'] + '000'))))
                        ],
                      ),
                    ),
                  ),
                )),
              );
            }).toList(),
          );
  }
}
